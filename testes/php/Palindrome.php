<?php
namespace App\php;
/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Um palíndromo é uma palavra que pode ser lida  da mesma maneira da esquerda para direita, como ao contrário.
Exemplo: ASA.

Faça uma função que ao receber um input de uma palavra, string, verifica se é um palíndromo retornando verdadeiro ou falso.
O fato de um caracter ser maiúsculo ou mínusculo não deverá influenciar no resultado da função.

Exemplo: isPalindrome("Asa") deve retonar true.
*/




class Palindrome
{
    /**Removendo caracteres especiais e letras com acento e em caso de frase, remover espaços em branco */
    private static function santizeString(string $word): string
    {
        $word_replace = str_replace(" ","",preg_replace("/&([a-z])[a-z0-9]+;/i", "$1", htmlentities(trim($word))));
        return strtolower($word_replace);
    }

    /*Regra de nedódio para palindrome */
    public static function isPalindrome(string $word): bool
    {
        
        //Conversão de palavra ou frase
        $treated_word = self::santizeString($word);
        //Verificando palindromo
        if($treated_word === strrev($treated_word))
            return true;        
        return false;
    }
}

// echo Palindrome::isPalindrome('Deleveled');