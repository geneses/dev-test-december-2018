<?php

namespace App\php;
/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exempolo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].


*/

class FileOwners
{
    public static function groupByOwners(array $files): array
    {
        //Array de resultados
        $filesGroupBy = array();
        foreach ($files as $file => $user) {
            //Primeira inserção de usuário e arquivo no array
            if (!array_key_exists(ucfirst($user), $filesGroupBy)) {
                $filesGroupBy[ucfirst($user)] = [$file];
            } else {
                //Anexando mais arquivos ao usuário existente
                $data_array = $filesGroupBy[ucfirst($user)];
                array_push($data_array, $file);
                $filesGroupBy[ucfirst($user)] = $data_array;
            }
        }
        return $filesGroupBy;
    }
}

// $files = array(
//     "Input.txt" => "Jose",
//     "Code.py" => "Joao",
//     "Output.txt" => "Jose",
//     "Click.js" => "Maria",
//     "Out.php" => "maria",

// );
// var_dump(FileOwners::groupByOwners($files));
