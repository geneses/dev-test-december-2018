<?php

namespace App\rest;

/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

Postmon é uma API para consultar CEP e encomendas de maneira fácil.

Implemente uma função que recebe CEP e retorna todos os dados reltivos ao endereço correspondente.

Exemplo:

getAddressByCep('13566400') retorna:
{
	"bairro": "Vila Marina",
	"cidade": "São Carlos",
	"logradouro": "Rua Francisco Maricondi",
	"estado_info": {
	"area_km2": "248.221,996",
	"codigo_ibge": "35",
		"nome": "São Paulo"
	},
	"cep": "13566400",
	"cidade_info": {
		"area_km2": "1136,907",
		"codigo_ibge": "3548906"
	},
	"estado": "SP"
}



Documentação:
https://postmon.com.br/


*/

class CEP
{
	/**
	 * Método para requisição
	 */
	private static function requestCep(string $cep): string
	{
		//Contexto para ignorar retorno de erro 
		$context = stream_context_create([
			'http' => [
				'ignore_errors' => true,
				'method'        => 'GET',
			]
		]);
		//Retornando requisição
		return file_get_contents("https://api.postmon.com.br/v1/cep/{$cep}",false,$context);
	}
    public static function getAddressByCep(string $cep): string
    {
		//Removedo caracteres não numericos 
		$cepTratado = preg_replace('/[\D]/',"",$cep);
		
		//Validação de Cep
		if(strlen($cepTratado) != 8){
			http_response_code(400);
			return json_encode(['error' => 'Cep Inválido!'],JSON_UNESCAPED_UNICODE);
		}
		//Retorno de requisição
		$dataJson = self::requestCep($cepTratado);
		//Cep não encontrado 
		if(empty($dataJson)){
			http_response_code(404);
			return json_encode(['error' => 'Cep não encontrado!'],JSON_UNESCAPED_UNICODE);
		}
		
		return $dataJson;
		
    }
}

// CEP::getAddressByCep('45.200-130');
// var_dump(json_decode(CEP::getAddressByCep('13.566-400'),true));