// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.

function removeProperty(obj, prop) {
  //convertendo para string
  prop = String(prop);
  //procurando e deletando propriedado do objeto
  if (prop in obj) {
    delete obj[prop];
    return true;
  }
  return false;
}

function addProperty(prop) {
  //convertendo para string
  prop = String(prop);
  //procurando propriedade para não adicionar repetida e adicionando ao objeto
  if (!(prop in obj)) {
    obj[prop] = Math.ceil(Math.random() * 10);
    return true;
  }
  return false;
}
module.exports = { removeProperty, addProperty };

// let obj = new Object;
// obj.a = 15;
// obj.b = 20;

// console.log(
//   removeProperty(obj, 'a')
// );