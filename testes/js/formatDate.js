// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.

function formatDate(userDate) {
  /**limpando data */
  let data = /^(\d{2})[-\/](\d{2})[-\/](\d{4})$/.exec(userDate);

  //Formatp incorreto de dada
  if (data === null)
    return "Formato incorreto";
  /**
   * Segregando dados e criando um objeto de data
   */
  let dia = data[2];
  let mes = data[1] - 1;
  let ano = data[3];
  let createDate = new Date(ano, mes, dia);

  /**
   * Validando data
   */

  if (
    createDate.getDate() !== parseInt(dia) ||
    createDate.getMonth() !== parseInt(mes) ||
    createDate.getFullYear() !== parseInt(ano)
  )
    return "Data inválida";

  return ano + (mes + 1) + dia;
}
module.exports = formatDate;
// console.log(formatDate("12/31/2014"));
