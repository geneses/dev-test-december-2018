# Petiko - Processo seletivo.

Petiko é uma Startup com sede em São Carlos que atua no segmento Pet com milhares de clientes espalhados em todo o Brasil. Nosso objetivo é ajudar as pessoas a cuidarem melhor de seus animais de estimação e entregar felicidade. Para isso, contamos com diferentes canais e serviços:


* **BLOG.Petiko:** Portal de conteúdo que movimenta centenas de milhares de petlovers todos os meses
* **BOX.Petiko:** Maior clube de assinaturas Pet da américa latina. Entregamos mensalmente uma caixinhas com mimos e novidades para nossos assinantes
* **SHOP.Petiko:** Plataforma de compra exclusivo para assinantes BOX.Petiko

# Sobre a vaga!

Queremos um dev talentoso, motivado e que esteja disposto a aprender diariamente. Que seja multidiciplinar, e encare o desafio de transformar-se em um **Full Stack**.
Você irá trabalhar com:

* PHP, Laravel
* MySQL e bancos não relacionais
* Integrações de plataformas via REST API's
* Desenvolvimento de ferramentas de gestão e análises de processos
* Métodos de pagamento online e logística
* Usabilidade, testes e desenvolvimento de novas plataformas de vendas

**A vaga:**

* **Desenvolvedor Junior, Backend PHP.**
* **Presencial, Sâo Carlos SP**
* **Home Office**
* **Formato de contratação CLT.**

# O Processo seletivo - ETAPA 1
Para participar do processo seletivo, convidamos você a resolver a primeira etapa do processo seletivo.

**Dê um fork neste repositório e resolva os testes**.
Assim que **terminar**, nos envie o link para o bitbucket **do projeto com as respostas** para o e-mail dev@petiko.com.br.

Nesta etapa o objetivo é analisar um pouco de sua familiaridade com algorítimos e programação.
Cada teste possui um peso. Você não é obrigado a entregar todos os testes resolvidos, porém, ele será utilizado como critério para a eleição a próxima fase.

Boa sorte!!!

# Resoluções

Para facilitar um pouco as impementações, foram criados alguns testes unitários para as seguintes soluções:

* PHP
    * FileOwners
    * LeagueTable
    * Palindrome
    * Cep ( Vale lemnrar que alguns testes nesta resolução são meramente inlustrativo).
* JS
    * FormatDate
    * RemoveGalery

As tecnologias utilizadas para executar os testes são o PHPUnit e Jest, para php e javascript respectivamente.

Os testes em Javascript são executados em uma instância nodejs, e com o intuito de facilitar a execução dos mesmos, foi criado um arquivo de configurações do docker com as seguintes instâncias:

* App
    * Container com o php, nodejs, npm e composer.
* Nginx
* Mysql
* PhpMyAdmin

Para a execução dos containers, é necessário ter o docker e o docker composer instalados na maquina, e caso queira, alterar algumas configurações no arquivo .env.

Para executar os testes, acesse o container app e execute:

* PHP
    * composer test
* Node
    * npm test

Para as resoluções SQL, foram anexados dois arquivos sql, um com o banco e o segundo com as consultas solicitadas.

