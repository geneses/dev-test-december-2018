<?php

namespace Tests\Php;

use PHPUnit\Framework\TestCase;

use App\rest\CEP;

class CepTest extends TestCase
{

    /**teste de digitos inválidos */
    public function testShouldReturnFalseForInvalidDigits()
    {
        $json_return = json_encode(['error' => 'Cep Inválido!'],JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($json_return,CEP::getAddressByCep('455540847036'));
    }

    /**
     * O teste abaixo é meramente ilusrativo para este cenário de seleção,
     * já que não é uma boa prática a execução de testes unitários, utilizando uma api externa.
     */
    
    /**teste de cep não encotrado */
    public function testShouldReturnFalseForZipNotFound()
    {
        $json_return = json_encode(['error' => 'Cep não encontrado!'],JSON_UNESCAPED_UNICODE);
        $this->assertJsonStringEqualsJsonString($json_return,CEP::getAddressByCep('45200000'));
    }


}
