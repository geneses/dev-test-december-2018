<?php

namespace Tests\Php;

use PHPUnit\Framework\TestCase;

use App\php\LeagueTable;

class LeagueTableTest extends TestCase
{
    /**
     * Caso de teste para retorno com cris em primeiro lugar
     */
    public function testShouldReturnCris()
    {
        $table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
        $table->recordResult('Mike', 2);
        $table->recordResult('Mike', 3);
        $table->recordResult('Arnold', 5);
        $table->recordResult('Chris', 5);

        $this->assertEquals('Chris', $table->playerRank(1));
    }


    /**
     * Caso de teste para retorno com Arnold em primeiro lugar
     */
    public function testShouldReturnArnold()
    {
        $table = new LeagueTable(array('Mike','Arnold' ,'Chris'));
        $table->recordResult('Mike', 2);
        $table->recordResult('Mike', 3);
        $table->recordResult('Arnold', 5);
        $table->recordResult('Chris', 5);

        $this->assertEquals('Arnold', $table->playerRank(1));
    }

    /**
     * Caso de teste para não retorno com Mike em primeiro lugar
     */
    public function testShouldNotReturnMike()
    {
        $table = new LeagueTable(array('Mike','Arnold' ,'Chris'));
        $table->recordResult('Mike', 2);
        $table->recordResult('Mike', 3);
        $table->recordResult('Arnold', 5);
        $table->recordResult('Chris', 5);

        $this->assertNotEquals('Mike', $table->playerRank(1));
    }

     /**
     * Caso de teste para não retorno com Mike em segundo lugar
     */
    public function testShouldReturnMike()
    {
        $table = new LeagueTable(array('Mike','Arnold' ,'Chris'));
        $table->recordResult('Mike', 2);
        $table->recordResult('Mike', 3);
        $table->recordResult('Arnold', 5);
        $table->recordResult('Chris', 5);

        $this->assertNotEquals('Mike', $table->playerRank(2));
    }

     /**
     * Caso de teste para entrada inválida
     */
    public function testShouldReturnErrorMessage()
    {
        $table = new LeagueTable(array('Mike','Arnold' ,'Chris'));
        $table->recordResult('Mike', 2);
        $table->recordResult('Mike', 3);
        $table->recordResult('Arnold', 5);
        $table->recordResult('Chris', 5);

        $this->assertEquals('Posição de ranking inválida', $table->playerRank(0));
    }
}
