<?php
namespace Tests\Php;

use PHPUnit\Framework\TestCase;
use App\php\Palindrome;

class PalindromeTest extends TestCase
{

    /**métoro de execução da regra de négocio e base lógica para algoritmo de testes */
    private function runIsPalindrome(array $datas): int
    {
        $count = 0;
        foreach($datas as $data){
            if(Palindrome::isPalindrome($data))
                $count++;
        }
        return $count;
    }
    /**
     * Casos de sucesso com palavras
     */
    public function testShouldReturnTrueForWords()
    {

        //Array de palavras de teste
        $words = array(
            'mutum',
            'ARARA',
            'omIssÍSsimo',
            'aNÃ'
        );
        
        $this->assertEquals(count($words),$this->runIsPalindrome($words));
    }

    /**
     * Casos de sucesso com frases
     */
    public function testShouldReturnTrueForPhrases()
    {
        //Array de frases de teste
        $Phrases = array(
            'Roma me tem amor',
            'AnOtArAm a daTa da MaRaToNa',
            'SaÍrAm o tiO E oito marias',
            'LAÇO BACANA PARA PANACA BOÇAL'
        );
        
        $this->assertEquals(count($Phrases),$this->runIsPalindrome($Phrases));
    }

    /**Casos de erro com palavras */
    public function testShouldReturnFalseForWords()
    {

        /*Array de palavras de teste,
         em caso de mesclar com palavras corretas, é obrigatório, ao menos, uma palavra que não seja palindrome
        */
        $words = array(
            'casa',
            'RATO',
            'CoMiDa',
            'cachaça'
        );
        
        $this->assertNotEquals(count($words),$this->runIsPalindrome($words));
    }

    /**
     * Casos de erro com frases
     */
    public function testShouldReturnFalseForPhrases()
    {
        /*
        Array de Frases de teste,
         em caso de mesclar com frases corretas, é obrigatório, ao menos, uma frase que não seja palindrome
        */
        $Phrases = array(
            'A casa no campo',
            'CoRREndo meia-maratona',
            'SaÍrAm o tiO E oito marias',
            'SÍRAM DAQUI PARA LÁ'
        );
        
        $this->assertNotEquals(count($Phrases),$this->runIsPalindrome($Phrases));
    }
}
