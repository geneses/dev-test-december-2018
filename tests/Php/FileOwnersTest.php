<?php

namespace Tests\Php;

use PHPUnit\Framework\TestCase;

use App\php\FileOwners;

class FileOwnersTest extends TestCase
{
    /**
     * Caso de teste para dados simples
     */
    public function testShouldReturnTrueForArrayData()
    {
        //Valores de entrada
        $data = array(
            "Input.txt" => "Jose",
            "Code.py" => "Joao",
            "Output.txt" => "Jose",
            "Click.js" => "Maria",
            "Out.php" => "maria",
        );

        //valores de saída
        $outData = array(
            'Jose' => ["Input.txt", "Output.txt"],
            'Joao' => ["Code.py"],
            "Maria" => ["Click.js","Out.php"]
        );

        $this->assertEquals($outData,FileOwners::groupByOwners($data));
    }
    /**
     * Caso de teste para nomes de usuários compostos
     */
    public function testShouldReturnTrueForCompositUsernames()
    {
        //Valores de entrada
        $data = array(
            "Input.txt" => "José Riquelme",
            "Code.py" => "Joao Carlos",
            "Output.txt" => "Jose",
            "Click.js" => "Maria",
            "Out.php" => "maria",
        );

        //valores de saída
        $outData = array(
            'José Riquelme' => ["Input.txt"],
            'Joao Carlos' => ["Code.py"],
            'Jose' => ['Output.txt'],
            "Maria" => ["Click.js","Out.php"]
        );

        $this->assertEquals($outData,FileOwners::groupByOwners($data));
    }

    /**
     * Caso erro para dados diferente ao esperado
     */
    public function testShouldReturnFalseForDifferentReturn(){
        //Valores de entrada
        $data = array(
            "Input.txt" => "José Riquelme",
            "Code.py" => "Joao Carlos",
            "Click.js" => "Maria",
            "Out.php" => "maria",
        );

        //valores de saída
        $outData = array(
            'José Riquelme' => ["Input.txt"],
            'Joao Carlos' => ["Code.py"],
            'Jose' => ['Output.txt'],
        );

        $this->assertNotEquals($outData,FileOwners::groupByOwners($data));
    }
     
}
