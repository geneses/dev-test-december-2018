const FormatDate = require('../../testes/js/formatDate');

/**Condição de sucesso */
test('should return the date in YYYYMMDD format', () => {
    expect(FormatDate("12/31/2014")).toBe("20141231");
});
/**Erro de formato de data */
test('should return error for date format', () => {
    expect(FormatDate("121/31/q22014")).toBe("Formato incorreto");
});

/**Erro de data inválida */
test('should return error for invalid date', () => {
    expect(FormatDate("12/34/2014")).toBe("Data inválida");
});