const { removeProperty } = require("../../testes/js/removeProperty");

/**Condição de sucesso */
describe('Success stories', () => {
    test('should return true for removal', () => {
        var obj = new Object;
        obj.a = 15;
        obj.b = 20;
        expect(removeProperty(obj, 'a')).toEqual(true);
    });
});
